//
//  CreateFitnesseClassTests.swift
//  FitnesseClassGenerator
//
//  Created by Richard Moult on 30/04/2016.
//  Copyright © 2016 TrickySquirrel. All rights reserved.
//

import XCTest
@testable import FitnesseClassGenerator


class CreateFitnesseClassTests: XCTestCase {

    override func setUp() {
        super.setUp()
    }

    
    override func tearDown() {
        super.tearDown()
    }

    // MARK: Decision Table

    func testCreateFileFromTable_DecisionTable_noProperties_outputCorrectString() {

        let expectedString = stringFromTemplateBundle("TestTemplateDTSwift")

        let table = Table(type: .decisionTable,
                          name: "ClassName",
                          inputType: .None,
                          properties: ["propertyA", "propertyB"],
                          propertyInputOnly: false)

        let result = newCodeStringFromTable(table)

        XCTAssertEqual(result, expectedString)
    }

    
    func testCreateFileFromTable_DecisionTable_1Properties_outputCorrectString() {

        let expectedString = stringFromTemplateBundle("TestTemplateDT1PropertySwift")

        let table = Table(type: .decisionTable,
                          name: "ClassName",
                          inputType: .One,
                          properties: ["propertyA", "propertyB"],
                          propertyInputOnly: false)

        let result = newCodeStringFromTable(table)

        XCTAssertEqual(result, expectedString)
    }


    func testCreateFileFromTable_DecisionTable_2Properties_outputCorrectString() {

        let expectedString = stringFromTemplateBundle("TestTemplateDT2PropertySwift")

        let table = Table(type: .decisionTable,
                          name: "ClassName",
                          inputType: .Many,
                          properties: ["propertyA", "propertyB"],
                          propertyInputOnly: false)

        let result = newCodeStringFromTable(table)

        XCTAssertEqual(result, expectedString)
    }


    // MARK: Decision Table Inputs Only

    func testCreateFileFromTable_DecisionTable_2Properties_InputsOnly_outputCorrectString() {

        let expectedString = stringFromTemplateBundle("TestTemplateDTPropertyInputOnlySwift")

        let table = Table(type: .decisionTable,
                          name: "ClassName",
                          inputType: .One,
                          properties: ["propertyA", "propertyC"],
                          propertyInputOnly: true)

        let result = newCodeStringFromTable(table)

        XCTAssertEqual(result, expectedString)
    }

    
    // MARK: Query Table or Ordered Query Table

    func testCreateFileFromTable_QueryTable_outputCorrectString() {

        let expectedString = stringFromTemplateBundle("TestTemplateQuerySwift")

        let table = Table(type: .queryTable,
                          name: "ClassName",
                          inputType: .One,
                          properties: ["propertyA", "propertyB"],
                          propertyInputOnly: false)

        let result = newCodeStringFromTable(table)

        XCTAssertEqual(result, expectedString)
    }


    // MARK: Helpers

    func stringFromTemplateBundle(fileName: String) -> String? {

        if let filepath = NSBundle.mainBundle().pathForResource(fileName, ofType: "res") {
            do {
                let contents = try NSString(contentsOfFile: filepath, usedEncoding: nil) as String
                return contents
            } catch {
                XCTFail()
            }
        } else {
            XCTFail()
        }
        return ""
    }

}
