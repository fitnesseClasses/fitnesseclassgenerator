//
//  Created by Richard Moult on 29/04/2016.
//  Copyright © 2016 TrickySquirrel. All rights reserved.
//

import XCTest
@testable import FitnesseClassGenerator


class TableStringConverterTests: XCTestCase {


    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    // MARK: Table Header Errors

    func testTableStringConvert_emptyInput_throwsFormatError() {

        XCTAssertThrowsError( try callTableStringToDataStruct(""), "") { (error) in
            XCTAssertEqual(error as? ValidateErrors, ValidateErrors.tableIncorrectFormat)
        }
    }

    func testTableStringConvert_noTableInput_throwsFormatError() {

        XCTAssertThrowsError( try callTableStringToDataStruct("jdkjd dkjdkf"), "") { (error) in
            XCTAssertEqual(error as? ValidateErrors, ValidateErrors.tableIncorrectFormat)
        }
    }

    func testTableStringConvert_incorrectStartInput_throwsFormatError() {

        XCTAssertThrowsError( try callTableStringToDataStruct("|"), "") { (error) in
            XCTAssertEqual(error as? ValidateErrors, ValidateErrors.tableIncorrectFormat)
        }
    }

    func testTableStringConvert_correctStartNoEndInput_throwsFormatError() {

        XCTAssertThrowsError( try callTableStringToDataStruct("!|"), "") { (error) in
            XCTAssertEqual(error as? ValidateErrors, ValidateErrors.tableIncorrectFormat)
        }
    }

    func testTableStringConvert_tableWithNoClassNameInput_throwsFormatError() {

        XCTAssertThrowsError( try callTableStringToDataStruct("!||"), "") { (error) in
            XCTAssertEqual(error as? ValidateErrors, ValidateErrors.tableIncorrectFormat)
        }
    }

    func testTableStringConvert_tableWithNoTableNameInput_throwsTableTypeError() {

        XCTAssertThrowsError( try callTableStringToDataStruct("!|SwiftClass|"), "") { (error) in
            XCTAssertEqual(error as? ValidateErrors, ValidateErrors.tableNameMissing)
        }
    }

    func testTableStringConvert_tableWithNoTableNameInput_throwsFormatError() {

        XCTAssertThrowsError( try callTableStringToDataStruct("!|DT:|"), "") { (error) in
            XCTAssertEqual(error as? ValidateErrors, ValidateErrors.tableClassMissing)
        }
    }

    // MARK: Decision Table

    func testTableStringConvert_DTWithTypeAndName_returnsTypeAndName() {

        let result = try? tableStringToDataStruct("!|DT:ClassName|")
        XCTAssertEqual(result?.name, "ClassName")
        XCTAssertEqual(result?.type, TableType.decisionTable)
    }

    func testTableStringConvert_DTWithTypeAndNameSpaces_returnsTypeAndName() {

        let result = try? tableStringToDataStruct("!|DT: Class name |")
        XCTAssertEqual(result?.name, "ClassName")
        XCTAssertEqual(result?.type, TableType.decisionTable)
    }

    func testTableStringConvert_DTWithTypeAndNameBrackets_returnsTypeAndName() {

        let result = try? tableStringToDataStruct("!|DT: (CEF) Class name |")
        XCTAssertEqual(result?.name, "CEFClassName")
        XCTAssertEqual(result?.type, TableType.decisionTable)
    }

    // MARK: Query Table

    func testTableStringConvert_QueryTableWithTypeAndName_returnsTypeAndName() {

        let result = try? tableStringToDataStruct("!| Query: Class name |")
        XCTAssertEqual(result?.name, "ClassName")
        XCTAssertEqual(result?.type, TableType.queryTable)
    }

    // MARK: Ordered Query Table

    func testTableStringConvert_OrderedQueryTableWithTypeAndName_returnsTypeAndName() {

        let result = try? tableStringToDataStruct("!| Ordered Query: Class name |")
        XCTAssertEqual(result?.name, "ClassName")
        XCTAssertEqual(result?.type, TableType.queryTable)
    }

    // MARK: Class Inputs

    func testTableStringConvert_classWithNOInput_returnsInitWithStringTrue() {

        let result = try? tableStringToDataStruct("!| Ordered Query: Class name |")
        XCTAssertEqual(result?.name, "ClassName")
        XCTAssertTrue(result!.inputType == .None)
    }

    func testTableStringConvert_classWith1Input_returnsInitWithStringTrue() {

        let result = try? tableStringToDataStruct("!| Ordered Query: Class name | bob |")
        XCTAssertEqual(result?.name, "ClassName")
        XCTAssertTrue(result!.inputType == .One)
    }

    func testTableStringConvert_classWith2Inputs_returnsInitWithStringTrue() {

        let result = try? tableStringToDataStruct("!| Ordered Query: Class name | bob | fred |")
        XCTAssertEqual(result?.name, "ClassName")
        XCTAssertTrue(result!.inputType == .Many)
    }

    // MARK: Property Names

    func testTableStringConvert_incompletePropertyRow_throwsError() {

        XCTAssertThrowsError( try callTableStringToDataStruct("!|DT:ClassName|\n|"), "") { (error) in
            XCTAssertEqual(error as? ValidateErrors, ValidateErrors.tableIncorrectFormat)
        }
    }

    func testTableStringConvert_emptyPropertyRow_returnsNoProperties() {

        let result = try? tableStringToDataStruct("!|DT: Class Name |\n||")
        XCTAssertNil(result?.properties)
    }

    func testTableStringConvert_1PropertyRow_returns2Properties() {

        let result = try? tableStringToDataStruct("!|DT: Class Name |\n|propertyA|")
        XCTAssertEqual(result?.properties?.count, 1)
        XCTAssertEqual(result?.properties?[safe:0], "propertyA")
    }

    func testTableStringConvert_2PropertiesRow_returns2Properties() {

        let result = try? tableStringToDataStruct("!|DT: Class Name |\n|propertyA|propertyB?|")
        XCTAssertEqual(result?.properties?.count, 2)
        XCTAssertEqual(result?.properties?[safe:0], "propertyA")
        XCTAssertEqual(result?.properties?[safe:1], "propertyB")
    }

    func testTableStringConvert_2PropertiesRowWithSpaces_returns2Properties() {

        let result = try? tableStringToDataStruct("!|DT: Class Name |\n| property a | property B? |")
        XCTAssertEqual(result?.properties?.count, 2)
        XCTAssertEqual(result?.properties?[safe:0], "propertyA")
        XCTAssertEqual(result?.properties?[safe:1], "propertyB")
    }

    func testTableStringConvert_1Property1CommentRowWithSpaces_returnsTypeAndName() {

        let result = try? tableStringToDataStruct("!|DT: Class Name |\n| property a | # comment |")
        XCTAssertEqual(result?.properties?.count, 1)
        XCTAssertEqual(result?.properties?[safe:0], "propertyA")
    }

    // MARK: Properties Setters Only

    func testTableStringConvert_DTSetterAndGetterProperties_returnsInputOnlyFalse() {

        let result = try? tableStringToDataStruct("!|DT: Class Name |\n| property a | property B? |")
        XCTAssertFalse(result!.propertyInputOnly)
    }

    func testTableStringConvert_DTSetterOnlyProperties_returnsInputOnlyTrue() {

        let result = try? tableStringToDataStruct("!|DT: Class Name |\n| property a | property B |")
        XCTAssertTrue(result!.propertyInputOnly)
    }

    // MARK: Helpers

    func callTableStringToDataStruct(string: String) throws {

        try tableStringToDataStruct(string)
    }

}
