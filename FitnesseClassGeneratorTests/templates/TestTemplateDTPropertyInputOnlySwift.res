//
//  Created by FitnesseBridgeGenerator
//


import Foundation


struct ClassNameData {
    var propertyA: NSString
    var propertyC: NSString

}

var staticClassNameDataSet = [NSString: [ClassNameData]]()



@objc(ClassName)

class ClassName: NSObject {

    // Id to identify the data in the staticDataSet
    var staticDataKey: NSString
    // Array of table rows
    var ClassNameDataSet = [ClassNameData]()


    // Properties
    var propertyA: NSString?
    var propertyC: NSString?


    init(string: NSString) {
        self.staticDataKey = string
        super.init()
    }

    // Called once before the rows are processed
    func beginTable() {
    }

    // Called once, just after the last row has been processed.  Last function to be called
    func endTable() {
        staticClassNameDataSet[self.staticDataKey] = ClassNameDataSet
    }

    // Called once for each row before any set or output functions are called
    func reset() {
    }

    // Called once for each row just after all the set functions have been called, and just before the first output function (if any) are called
    func execute() {
        let data = ClassNameData(propertyA: propertyA!, propertyC: propertyC!)
        self.ClassNameDataSet.append(data)
    }

    // Called by other classes to extract the data
    class func getStaticClassNameDataSet(staticDataKey: NSString) -> [ClassNameData]? {
        return staticClassNameDataSet[staticDataKey]
    }

}
