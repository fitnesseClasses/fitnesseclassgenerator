//
//  Created by FitnesseBridgeGenerator
//


import Foundation


@objc(ClassName)

class ClassName: NSObject {

    // Properties
    var propertyA: NSString?
    var propertyB: NSString?


    init() {
        super.init()
    }

    // Called once before the rows are processed
    func beginTable() {
    }

    // Called once, just after the last row has been processed.  Last function to be called
    func endTable() {
    }

    // Called once for each row before any set or output functions are called
    func reset() {
    }

    // Called once for each row just after all the set functions have been called, and just before the first output function (if any) are called
    func execute() {
    }

}
