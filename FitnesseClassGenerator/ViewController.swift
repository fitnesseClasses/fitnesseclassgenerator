//
//  ViewController.swift
//  FitnesseClassGenerator
//
//  Created by Richard Moult on 07/05/2016.
//  Copyright © 2016 TrickySquirrel. All rights reserved.
//

import Cocoa

class ViewController: NSViewController {


    @IBOutlet var tableTextView: NSTextView?


    override var representedObject: AnyObject? {
        didSet {
            // Update the view, if already loaded.
        }
    }


    @IBAction func helpButtonPresses(sender: AnyObject) {
        NSWorkspace.sharedWorkspace().openURL(NSURL(string: "http://www.fitnesse.org/FitNesse.UserGuide.WritingAcceptanceTests.SliM")!)
    }


    @IBAction func generateSwiftClassButtonSelected(sender: AnyObject) {

        guard let tableString = tableTextView?.string else { return }
        do {
            let tableData = try tableStringToDataStruct(tableString)
            let tableToClassString = newCodeStringFromTable(tableData)
            saveTableString(tableToClassString, fileNameWithExtension: "example.swift")
        }
        catch {
            print("\(error)")
            let validateError = error as? ValidateErrors
            showAlert( validateError!.userErrorMessage() )
        }
    }

    func showAlert(message: String) {

        let alert = NSAlert()
        alert.messageText = "Warning"
        alert.addButtonWithTitle("Yes")
        alert.informativeText = message

        alert.beginSheetModalForWindow(self.view.window!, completionHandler: { (returnCode) -> Void in
        })
    }

}

