//
//  Created by FitnesseBridgeGenerator
//


import Foundation


struct #classnameData {
#properties
}

var static#classnameDataSet = [NSString: [#classnameData]]()



@objc(#classname)

class #classname: NSObject {

    // Id to identify the data in the staticDataSet
    var staticDataKey: NSString
    // Array of table rows
    var #classnameDataSet = [#classnameData]()


    // Properties
#optionalProperties

    init(#inputType) {
        self.staticDataKey = string
        super.init()
    }

    // Called once before the rows are processed
    func beginTable() {
    }

    // Called once, just after the last row has been processed.  Last function to be called
    func endTable() {
        static#classnameDataSet[self.staticDataKey] = #classnameDataSet
    }

    // Called once for each row before any set or output functions are called
    func reset() {
    }

    // Called once for each row just after all the set functions have been called, and just before the first output function (if any) are called
    func execute() {
        let data = #classnameData(#keyValueProperties)
        self.#classnameDataSet.append(data)
    }

    // Called by other classes to extract the data
    class func getStatic#classnameDataSet(staticDataKey: NSString) -> [#classnameData]? {
        return static#classnameDataSet[staticDataKey]
    }

}
