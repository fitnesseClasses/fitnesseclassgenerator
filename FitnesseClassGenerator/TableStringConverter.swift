//
//  Created by Richard Moult on 29/04/2016.
//  Copyright © 2016 TrickySquirrel. All rights reserved.
//

import Foundation


enum ValidateErrors: ErrorType {

    case tableIncorrectFormat
    case tableNameMissing
    case tableClassMissing

    func userErrorMessage() -> String {
        switch self {
        case .tableIncorrectFormat:
            return "Table has incorrect formatting please check start of table has !| and the end of each row has |"
        case .tableNameMissing:
            return  "Table does not contain known name, (DT) e.g. !|DT:xxx|"
        case .tableClassMissing:
            return  "Table does not contain a class name e.g. !|DT:ClassName|"
        }
    }
}


enum TableType: String {

    case decisionTable = "DT:"
    case queryTable = "Query:"
//    case scriptTable

    func templateFileName(propertiesInputOnly: Bool) -> String {

        switch self {
        case decisionTable:
            return propertiesInputOnly ? "TemplateDTPropertyInputOnlySwift" : "TemplateDTSwift"
        case queryTable:
            return "TemplateQueryTableSwift"
        }
    }
}


enum TableInputType {

    case None
    case One
    case Many
}


struct Table {

    var type: TableType
    var name: String
    var inputType: TableInputType
    var properties: [String]?
    var propertyInputOnly: Bool
}


func tableStringToDataStruct(string: String) throws -> Table {

    let lines:[String] = string.lines
    let result = try readTableHeader(lines)
    let tableProperties = try readTableProperties(lines)
    let table = Table(type: result.type,
                      name: result.name,
                      inputType: result.inputType,
                      properties: tableProperties.properties,
                      propertyInputOnly: tableProperties.inputOnly)
    return table
}

// MARK: Table Header

func readTableHeader(lines: [String]) throws -> (type: TableType, name: String, inputType: TableInputType) {

    return try parseTableTypeAndName(lines.first)
}


func parseTableTypeAndName(string: String?) throws -> (type: TableType, name: String, inputType: TableInputType) {

    guard let tableHeader = string else { throw ValidateErrors.tableIncorrectFormat }
    guard (validateTableHeaderFormat(tableHeader) == true) else { throw ValidateErrors.tableIncorrectFormat }

    let array = tableHeader.splitByString("|")

    guard let tableType = findTableType(array[1]) else { throw ValidateErrors.tableNameMissing }
    guard let tableName = findTableName(array[1], tableType: tableType) else { throw ValidateErrors.tableClassMissing }
    let tableInputType = findTableInputType(array)
    return (tableType, tableName.stripNoneAlphanumericCharacters(), tableInputType)
}


func findTableInputType(strings: [String]) -> TableInputType {

    if strings.count <= 2 {
        return .None
    }
    else if strings.count == 3 {
        return .One
    }
    return .Many
}


func findTableType(string: String) -> TableType? {

    if string.contains(TableType.decisionTable.rawValue) {
        return TableType.decisionTable
    }
    else if string.contains(TableType.queryTable.rawValue) {
        return TableType.queryTable
    }
    return nil
}


func findTableName(string: String, tableType: TableType) -> String? {

    var possibleName = ""

    if let startIndex = string.rangeOfString(tableType.rawValue) {

        if startIndex.endIndex < string.characters.endIndex {
            let r = startIndex.endIndex..<string.characters.endIndex
            possibleName = string[r].toWikiWord()
        }
    }

    if possibleName.isClassName() {
        return possibleName
    }
    return nil
}


func validateTableHeaderFormat(string: String?) -> Bool {

    guard let validString = string else { return false }
    guard validString.isValidTableStart() else { return false }
    guard validString.isValidEndRow() else { return false }
    guard validString.isMinimumTableLength() else { return false }
    return true
}


// MARK: Table Properties

func readTableProperties(lines: [String]) throws -> (properties: [String]?, inputOnly: Bool) {

    return try parseTableProperties(lines[safe:1])
}


func parseTableProperties(string: String?) throws -> (properties: [String]?, inputOnly: Bool) {

    guard let row = string else { return (nil, false) }
    guard (validateRowFormat(row) == true) else { throw ValidateErrors.tableIncorrectFormat }

    let properties = row.splitByString("|").map{ $0.toWikiWord() }.filter{ !$0.isCommentProperty() }
    let strippedProperties = properties.map{ $0.stripNoneAlphanumericCharacters() }
    return ( strippedProperties, tablePropertiesInputOnly(properties) )
}


func tablePropertiesInputOnly(array: [String]) -> Bool {

    let getters = array.filter{ $0.isGetterProperty() }
    return getters.count > 0 ? false : true
}


func validateRowFormat(string: String?) -> Bool {

    guard let row = string else { return false }
    guard row.isValidStartRow() else { return false }
    guard row.isValidEndRow() else { return false }
    guard row.isMinimumRowLength() else { return false }
    return true
}
