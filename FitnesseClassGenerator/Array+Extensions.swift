//
//  Array+Extensions.swift
//  FitnesseClassGenerator
//
//  Created by Richard Moult on 29/04/2016.
//  Copyright © 2016 TrickySquirrel. All rights reserved.
//

import Foundation


extension Array {

    subscript (safe index: Int) -> Element? {
        return indices ~= index ? self[index] : nil
    }
    
}