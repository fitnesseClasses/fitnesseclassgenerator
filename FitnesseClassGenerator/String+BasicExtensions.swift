//
//  String+Extensions.swift
//  FitnesseClassGenerator
//
//  Created by Richard Moult on 29/04/2016.
//  Copyright © 2016 TrickySquirrel. All rights reserved.
//

import Foundation


extension String {

    var lines:[String] {
        var result:[String] = []
        enumerateLines{ result.append($0.line) }
        return result
    }

    func length() -> Int {
        return self.characters.count
    }

    func contains(find:String) -> Bool {
        return self.rangeOfString(find) != nil
    }

}

