//
//  String+TableExtensions.swift
//  FitnesseClassGenerator
//
//  Created by Richard Moult on 03/05/2016.
//  Copyright © 2016 TrickySquirrel. All rights reserved.
//

import Foundation


extension String {

    var fitTableStart: String { return "!|" }
    var fitRowSeparator: String { return "|" }


    func isMinimumTableLength() -> Bool {
        if self.length() <= (fitTableStart.length() + fitRowSeparator.length()) {
            return false
        }
        return true
    }

    func isMinimumRowLength() -> Bool {
        if self.length() <= (fitRowSeparator.length() + fitRowSeparator.length()) {
            return false
        }
        return true
    }

    func isClassName() -> Bool {
        guard self.length() > 3 else { return false }
        return true
    }

    func isValidTableStart() -> Bool {
        guard self.tableStart() == fitTableStart else { return false }
        return true
    }

    func isValidStartRow() -> Bool {
        guard self.rowStart() == fitRowSeparator else { return false }
        return true
    }

    func isValidEndRow() -> Bool {
        guard self.rowEnd() == fitRowSeparator else { return false }
        return true
    }

    func tableStart() -> String {
        guard self.characters.count >= fitTableStart.characters.count else { return "" }
        let r = self.startIndex.advancedBy(0)..<self.startIndex.advancedBy(2)
        return self[r]
    }

    func rowStart() -> String {
        let r = self.startIndex..<self.startIndex.advancedBy(1)
        return self[r]
    }

    func rowEnd() -> String {
        guard self.characters.count >= fitRowSeparator.characters.count else { return "" }
        let r = self.endIndex.advancedBy(-1)..<self.endIndex.advancedBy(0)
        return self[r]
    }

    func splitByString(string:String) -> [String] {
        return self.componentsSeparatedByString(string).flatMap({ (string) -> String? in
            if string.length() > 0 {
                return string
            }
            return nil
        })
    }

    func stripNoneAlphanumericCharacters() -> String {
        let unsafeChars = NSCharacterSet.alphanumericCharacterSet().invertedSet
        return self.componentsSeparatedByCharactersInSet(unsafeChars).joinWithSeparator("")
    }

    func toWikiWord() -> String {
        return self.splitByString(" ").reduce("", combine: {
            if $0.length() > 0 {
                return $0+$1.capitalizedString
            }
            else {
                return $0+$1
            }
        })
    }

    func isCommentProperty() -> Bool {
        if self.contains("#") {
            return true
        }
        return false
    }

    func isGetterProperty() -> Bool {
        if self.contains("?") {
            return true
        }
        return false
    }

}

