//
//  ExportTable.swift
//  FitnesseClassGenerator
//
//  Created by Richard Moult on 03/05/2016.
//  Copyright © 2016 TrickySquirrel. All rights reserved.
//

import Cocoa


func saveTableString(saveString: String, fileNameWithExtension: String) {

    let savePanel = NSSavePanel()
    savePanel.nameFieldStringValue = fileNameWithExtension

    savePanel.beginWithCompletionHandler { (result: Int) -> Void in

        if result == NSFileHandlingPanelOKButton {

            let exportedFileURL = savePanel.URL

            do {
                try saveString.writeToURL(exportedFileURL!, atomically: true, encoding: NSUTF8StringEncoding)
            }
            catch {
                print("\(error)")
            }
        }
    }
}
