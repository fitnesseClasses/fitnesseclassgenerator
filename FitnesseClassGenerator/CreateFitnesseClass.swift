//
//  CreateFitnesseClass.swift
//  FitnesseClassGenerator
//
//  Created by Richard Moult on 30/04/2016.
//  Copyright © 2016 TrickySquirrel. All rights reserved.
//

import Foundation



func newCodeStringFromTable(table: Table) -> String {

    var string = stringFromTemplateBundle(table.type.templateFileName(table.propertyInputOnly))
    string = string!.stringByReplacingOccurrencesOfString("#classname", withString: table.name)
    string = string!.stringByReplacingOccurrencesOfString("#inputType", withString: inputTypeString(table))
    string = string!.stringByReplacingOccurrencesOfString("#optionalProperties", withString: propertyString(table, optional:true))
    string = string!.stringByReplacingOccurrencesOfString("#properties", withString: propertyString(table, optional:false))
    string = string!.stringByReplacingOccurrencesOfString("#keyValueProperties", withString: keyValueProperties(table))
    return string!
}


func keyValueProperties(table: Table) -> String {

    guard table.propertyInputOnly else { return "" }
    guard let propertiesList = table.properties else { return "" }

    var string = ""
    for property in propertiesList {
        if string.length() > 0 {
            string += ", "
        }
        let str = "\(property): \(property)!"
        string += str
    }
    return string //"propertyA: propertyA!, propertyB: propertyB!"
}


func inputTypeString(table: Table) -> String {

    switch table.inputType {
    case .None:
        return ""
    case .One:
        return "string: NSString"
    case .Many:
        return "strings: [NSString]"
    }
}


func propertyString(table: Table, optional: Bool) -> String {

    guard let properties = table.properties else { return "" }
    var string = ""
    for property in properties {
        if optional==true {
            string += "    var \(property): NSString?\n"
        }
        else {
            string += "    var \(property): NSString\n"
        }
    }
    return string
}


// MARK: templates

func stringFromTemplateBundle(fileName: String) -> String? {

    if let filepath = NSBundle.mainBundle().pathForResource(fileName, ofType: "res") {
        do {
            let contents = try NSString(contentsOfFile: filepath, usedEncoding: nil) as String
            return contents
        } catch {
            print("could not open file")
        }
    } else {
        print("could not find file")
    }
    return ""
}
